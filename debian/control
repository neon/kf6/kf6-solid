Source: kf6-solid
Section: libs
Priority: optional
Maintainer: Jonathan Esk-Riddell <jr@jriddell.org>
Build-Depends: bison,
               cmake,
               debhelper-compat (= 13),
               doxygen,
               flex,
               graphviz,
               kf6-extra-cmake-modules,
               libimobiledevice-dev,
               libmount-dev,
               libudev-dev [linux-any],
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-tools-dev
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/frameworks/solid
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/solid
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/solid.git

Package: kf6-solid
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: media-player-info [linux-any], udisks2 [linux-any], upower
Breaks: libkf5solid-bin (<< 5.115.0-1)
Replaces: libkf5solid-bin (<< 5.115.0-1),
          libkf6solid6,
          libkf6solid-bin,
          libkf6solid-data,
          qml6-module-org-kde-solid,
          qtdeclarative6-kf6solid,
Priority: optional
Description: Qt library to query and control hardware
 Solid is a device integration framework. It provides a way of querying and
 interacting with hardware independently of the underlying operating system.
 .
 Solid is part of KDE Frameworks 6.
 .
 This package contains the solid-hardware6 tool.

Package: kf6-solid-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: kf6-solid (= ${binary:Version}),
         qt6-base-dev,
         qt6-declarative-dev,
         ${misc:Depends}
Replaces: libkf6solid-dev,
          libkf6solid-daoc,
Description: Qt library to query and control hardware
 Solid is a device integration framework. It provides a way of querying and
 interacting with hardware independently of the underlying operating system.
 .
 This package is part of KDE Frameworks 6.
 .
 This package contains the development files.

Package: libkf6solid6
Architecture: all
Depends: kf6-solid, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6solid-bin
Architecture: all
Depends: kf6-solid, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6solid6-data
Architecture: all
Depends: kf6-solid, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6solid-dev
Architecture: all
Depends: kf6-solid-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6solid-doc
Architecture: all
Depends: kf6-solid-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-org-kde-solid
Architecture: all
Depends: kf6-solid, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qtdeclarative6-kf6solid
Architecture: all
Depends: kf6-solid, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

